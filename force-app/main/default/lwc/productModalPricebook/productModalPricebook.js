import { LightningElement } from 'lwc';

export default class ProductModalPricebook extends LightningElement {
    name;
    description;

    handleChange(event) {
        if (event.target.dataset.name === 'name') {
            this.name = event.target.value;
        } else {
            this.description = event.target.value;
        }
    }

    closeModal() {
        const cancelEvent = new CustomEvent(
            'cancel'
        );
        this.dispatchEvent(cancelEvent);
    }

    submitDetails() {
        const saveEvent = new CustomEvent(
            'save',
            {
                detail: {
                    Name: this.name,
                    Description: this.description
                }
            }            
        );
        this.dispatchEvent(saveEvent);
    }
}