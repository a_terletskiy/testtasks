import { LightningElement } from 'lwc';

export default class ProductModalProduct extends LightningElement {

    name;
    description;
    sku;

    handleChange(event) {
        if (event.target.dataset.name === 'name') {
            this.name = event.target.value;
        } else if (event.target.dataset.name === 'description') {
            this.description = event.target.value;
        } else {
            this.sku = event.target.value;
        }
    }

    closeModal() {
        const cancelEvent = new CustomEvent(
            'cancel'
        );
        this.dispatchEvent(cancelEvent);
    }

    submitDetails() {
        const saveEvent = new CustomEvent(
            'save',
            {
                detail: {
                    Name: this.name,
                    Description: this.description,
                    StockKeepingUnit: this.sku
                }
            }            
        );
        this.dispatchEvent(saveEvent);
    }

}