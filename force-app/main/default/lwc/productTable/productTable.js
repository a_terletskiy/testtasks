import { LightningElement, wire, api, track } from 'lwc';
import { refreshApex } from '@salesforce/apex';
import { NavigationMixin } from 'lightning/navigation';
import getProducts from '@salesforce/apex/ProductController.getProducts';
import getPricebooks from '@salesforce/apex/ProductController.getPricebooks';
import getPricebookEntries from '@salesforce/apex/ProductController.getPricebookEntries';
import createProduct from '@salesforce/apex/ProductController.createProduct';
import createPricebook from '@salesforce/apex/ProductController.createPricebook';
import deleteProductWithPbes from '@salesforce/apex/ProductController.deleteProductWithPbes';
import saveProductsWithPbes from '@salesforce/apex/ProductController.saveProductsWithPbes';
import { showErrorToast, showSuccessfullToast } from 'c/utils';

export default class ProductTable extends NavigationMixin(LightningElement) {

    defaultColumns = [
        { label: 'Name', fieldName: 'Name', isPrice: false },
        { label: 'Description', fieldName: 'Description', isPrice: false },
        { label: 'Product SKU code', fieldName: 'StockKeepingUnit', isPrice: false }
    ];

    columns;

    searchTerm = '';

    _sourceData;

    _pricebooks;

    _pricebookEntries;

    @track
    pagedData = [];          

    currentPage = 1;

    maxPages = 1;

    disabledPreviousButton = false;
    disabledNextButton = false;
    loading = true;

    isModalProductOpen = false;

    isModalPricebookOpen = false;

    _pricebookUrls = [];

    urlsDidLoad = false;

    urlsCount = 0;

    get sourceData() {
        function compare( a, b ) {
            if ( a.Name.length < b.Name.length ){
                return -1;
            }
            if ( a.Name.length > b.Name.length ){
                return 1;
            }
            if ( a.Name < b.Name ){
                return -1;
            }
            if ( a.Name > b.Name ){
                return 1;
            }
            return 0;
        }

        if(!this._sourceData)
            return this._sourceData;

        let data = JSON.parse(JSON.stringify(this._sourceData)).sort(compare);

        if(this.searchTerm == '') 
            return data;

        let tempList = [];

        for(let record of data) {
            record.Name.toLowerCase().includes(this.searchTerm.toLowerCase()) && tempList.push(record);
        }

        return tempList;
    }

    get pricebookUrls() {
        return this._pricebookUrls;
    }
    set pricebookUrls(value) {
        this.pricebookRef = {
            type: 'standard__recordPage',
            attributes: {
                recordId: value,
                objectApiName: 'Pricebook2',
                actionName: 'view'
            }
        };
        this[NavigationMixin.GenerateUrl](this.pricebookRef)
            .then(url => { this._pricebookUrls[value] = url; this.urlsCount -= 1; this.checkForAllLoad(); });
    }

    checkForAllLoad() {
        if(this._sourceData && this._pricebookEntries && this._pricebooks && this.urlsCount == 0) {
            this.gotoPage(this.currentPage);
        }
    }

    handleSearchTermChange(event) {
        this.searchTerm = event.target.value;
        this.resetPaging();
	}

    @track
    wiredPricebooksResult = {};

    @wire(getPricebooks)
    pricebooksCallback(result) {
        this.wiredPricebooksResult = result;
        if (result.data) {
            this._pricebookUrls = [];
            this.urlsCount = 0;
            for(let pb of result.data) {
                this.urlsCount += 1;
                this.pricebookUrls = pb.Id;
            }
            this._pricebooks = result.data;
            this.checkForAllLoad();
        }
    }

    @track
    wiredPricebookEntriesResult = {};

    @wire(getPricebookEntries)
    pricebookEntriesCallback(result) {
        this.wiredPricebookEntriesResult = result;
        if (result.data) {
            this._pricebookEntries = result.data;
            this.checkForAllLoad();
        }
        else if (result.error)
            console.log(result.error);
    }
    
    @track
    wiredProductsResult = {};

    @wire(getProducts)
    productsCallback(result) {
        this.wiredProductsResult = result;
        
        if (result.data) {   
            this._sourceData = result.data;  
            this.checkForAllLoad();
        }
    }
   

    connectedCallback() {

        this.displayAmount = 10;
    }

    
    handleRefreshPrice() {
        refreshApex(this.wiredPricebookEntriesResult);  
    }

    @api resetPaging() {

        this.gotoPage(1);
    }

    @api 
    get displayAmount() {return this._displayAmount};     
    set displayAmount(value) {
        this._displayAmount = value;
        this.gotoPage(1);                             
    }
    _displayAmount;

    handleButtonNext() {

        var nextPage = this.currentPage + 1;
        var maxPages =  this.getMaxPages();

        if(nextPage > 0 && nextPage <= maxPages) {

            this.gotoPage(nextPage);
        }
    }

    handleButtonPrevious() {

        var nextPage = this.currentPage - 1;
        var maxPages =  this.getMaxPages();

        if(nextPage > 0 && nextPage <= maxPages) {

            this.gotoPage(nextPage);
        }
    }

    getMaxPages() {

        var result = 1;

        var arrayLength;

        var divideValue;

        if(this.sourceData) {

            arrayLength = this.sourceData.length;

            divideValue = arrayLength / this.displayAmount;

            result = Math.ceil(divideValue); 
        }

        this.maxPages = result;

        return result;
    }

    
    recordsInEdit = [];

    @track
    _notNullRecsCount = 0;

    get notNullRecsCount() {
        return this._notNullRecsCount;
    }
    set notNullRecsCount(records) {
        let count = 0;
        for(let id in records)
            records[id] != null && count++;
        this._notNullRecsCount = count;
    }

    handleChangeItem(event) {
        this.recordsInEdit[event.detail.newData.Id] = JSON.parse(JSON.stringify(event.detail));
        this.notNullRecsCount = this.recordsInEdit;
    }

    handleEndEdit(event) {
        this.recordsInEdit[event.detail] = null;
        this.notNullRecsCount = this.recordsInEdit;
    }

    handleDeleteItem(event) {
        let notStandardPbes = [];
        let standardPbes = [];
        const id = event.detail;
        for(let pbe of this._pricebookEntries)
            if(pbe.Product2Id == id){
                if(pbe.Pricebook2.IsStandard)
                    standardPbes.push(pbe);
                else
                    notStandardPbes.push(pbe);
            }
        this.deleteItem(id, standardPbes, notStandardPbes);
    }

    deleteItem(id, standardPbes, notStandardPbes) {
        deleteProductWithPbes({ productId: id, standardPbes: JSON.stringify(standardPbes), notStandardPbes: JSON.stringify(notStandardPbes) })
        .then(() => {      
            refreshApex(this.wiredProductsResult);  
            showSuccessfullToast(this, 'Product deleted');                               
        })
        .catch((error) => {
            console.log(error);
            showErrorToast(this, 'Error deleting Product');
        });          
    }

    get showSaveAllButton() {

        return this.notNullRecsCount > 1;
    }

    onSaveAllButtonClick() {
        let tempList = Object.values(this.recordsInEdit);        
        let productResult = [];
        let pbeResult = [];
        for(let item of tempList) {
            if(item != null) {

                productResult.push({
                    Id: item.newData.Id,
                    Name: item.newData.Name,
                    Description: item.newData.Description,
                    StockKeepingUnit: item.newData.StockKeepingUnit
                });

                for(let price of item.newData.prices) {
                    let id = null;
                    for(let pbe of this._pricebookEntries) {
                        if(pbe.Pricebook2Id == price.pricebookId && pbe.Product2Id == item.newData.Id)
                            id = pbe.Id;
                    }
                    if(id != null)
                        pbeResult.push({
                            Id: id,
                            Product2Id: item.newData.Id,
                            Pricebook2Id: price.pricebookId,
                            UnitPrice: price.price
                        });
                    else
                        pbeResult.push({
                            Product2Id: item.newData.Id,
                            Pricebook2Id: price.pricebookId,
                            UnitPrice: price.price
                        });
                }
            }
        }
        this.saveAll(productResult, pbeResult);
    }

    saveAll(products, pbes) {
        saveProductsWithPbes({ products: JSON.stringify(products), pbes: JSON.stringify(pbes) })
        .then(() => {         
            this.recordsInEdit = [];          
            this.notNullRecsCount = this.recordsInEdit; 
            this.template
            .querySelectorAll("c-product-item")
            .forEach(element => {
              element.isEditing = false;
            });
            showSuccessfullToast(this, 'Products updated');
            refreshApex(this.wiredProductsResult);  
            refreshApex(this.wiredPricebookEntriesResult);          
        })
        .catch((error) => {
            console.log(error);
            showErrorToast(this, 'Error updating Products');
        });      
    }

    newProduct(name, description, sku) {
        createProduct({ name: name, description: description, sku: sku })
            .then(() => {
                showSuccessfullToast(this, 'Product created');  
                this.isModalProductOpen = false;
                refreshApex(this.wiredProductsResult);
            })
            .catch((error) => {
                console.log(error);
                showErrorToast(this, 'Error creating product');
            });
    }

    newPricebook(name, description) {
        createPricebook({ name: name, description: description })
            .then(() => {
                showSuccessfullToast(this, 'Pricebook created');  
                this.isModalPricebookOpen = false;
                refreshApex(this.wiredPricebooksResult);
            })
            .catch((error) => {
                console.log(error);
                showErrorToast(this, 'Error creating Pricebook');
            });
    }

    getPricebookUrl(id) {
        return this.pricebookUrls[id];
    }

    showModalProduct() {
        this.isModalProductOpen = true;
    }

    showModalPricebook() {
        this.isModalPricebookOpen = true;
    }

    handleCreateProduct(event) {
        this.newProduct(event.detail.Name, event.detail.Description, event.detail.StockKeepingUnit);
    }

    handleCancelCreateProduct() {
        this.isModalProductOpen = false;
    }

    handleCreatePricebook(event) {
        this.newPricebook(event.detail.Name, event.detail.Description);
    }

    handleCancelCreatePricebook() {
        this.isModalPricebookOpen = false;
    }

    gotoPage(pageNumber) {

        var recordStartPosition, recordEndPosition;
        var i, arrayElement;      

        var maximumPages = this.maxPages;
        
        this.loading = true;

        maximumPages = this.getMaxPages();

        if( pageNumber > maximumPages || pageNumber < 0 ) {
            
            this.loading = false;
            if(pageNumber > maximumPages && maximumPages > 0)
                this.gotoPage(maximumPages);
            else    
                this.pagedData = [];
            return;
        }

        this.disabledPreviousButton = false;
        this.disabledNextButton = false;

        

        if(this.sourceData) {

            this.pagedData = [];

            recordStartPosition = this.displayAmount * (pageNumber - 1);

            recordEndPosition = recordStartPosition + parseInt(this.displayAmount, 10);

            for ( i = recordStartPosition; i < recordEndPosition; i++ ) {

                arrayElement = this.sourceData[i];

                if(arrayElement) {

                    this.pagedData.push(arrayElement);
                }
            }

            this.columns = [...this.defaultColumns];
            
            for(let pb of this._pricebooks) {
                this.columns.push({label: pb.Name, fieldName: pb.Id, isPrice: true, isStandard: pb.IsStandard, url: this.getPricebookUrl(pb.Id)});
            }

            this.columns.push({label: 'Edit/Save', fieldName: 'Edit', isPrice: false});
            this.columns.push({label: 'Delete/Cancel', fieldName: 'Delete', isPrice: false});
            
            let tempData = [];

            for(let product of this.pagedData) {
                let data = {...product, prices: []};
                for(let pbe of this._pricebookEntries)
                    if(pbe.Product2Id == data.Id)
                        data.prices.push({pricebookId: pbe.Pricebook2Id, price: pbe.UnitPrice, isStandard: pbe.Pricebook2.IsStandard});
                tempData.push(data);
            }

            this.pagedData = tempData;

            tempData = [];

            for(let product of this.pagedData) {
                if(this.recordsInEdit[product.Id] != null) {
                    let record = this.recordsInEdit[product.Id].newData;
                    record.oldData = this.recordsInEdit[product.Id].oldData;
                    record.inEdit = true;
                    tempData.push(record);
                }
                else {
                    let record = product;
                    record.inEdit = false;
                    tempData.push(record);
                }
            }

            this.pagedData = tempData;

            this.currentPage = pageNumber;

            if(maximumPages === this.currentPage) {
                
                this.disabledNextButton = true;
            }

            if(this.currentPage === 1) {

                this.disabledPreviousButton = true;
            }

            this.loading = false;
        }
    }

}