import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import createPriceBookEntry from '@salesforce/apex/ProductController.createPriceBookEntry';
import deletePricebookEntry from '@salesforce/apex/ProductController.deletePriceBookEntry';
import saveProductsWithPbes from '@salesforce/apex/ProductController.saveProductsWithPbes';
import { showSuccessfullToast, showErrorToast } from 'c/utils';

export default class ProductItem extends NavigationMixin(LightningElement) {
    
    @api
    get tabledata() {
        return this._tabledata;
    }
    set tabledata(value) {
        this._tabledata = value;
        this.prices = value.prices;
    }

    @api
    pbentries;

    _productUrl;

    get productUrl() {
        return this._productUrl;
    }
    set productUrl(value) {
        this.productRef = {
            type: 'standard__recordPage',
            attributes: {
                recordId: value,
                objectApiName: 'Product2',
                actionName: 'view'
            }
        };
        this[NavigationMixin.GenerateUrl](this.productRef)
            .then(url => this._productUrl = url);
    }

    @api
    get columns() {
        return this._columns;
    }
    set columns(value) {
        this._columns = value;
        this.pricebooks = [];
        for (let col of this._columns) {
            if(col.isPrice)
                this.pricebooks.push(col.fieldName);
        }
    }
    
    pricebooks = [];

    @track
    _columns;

    _tabledata;

    get prices() {
        return this.priceData;
    }
    set prices(value) {
        let prices = JSON.parse(JSON.stringify(value));
        this.priceData = [];
        let standard = false;
        let hasPrice = false;
        for(let col of this.columns)
            if(col.isPrice) {
                hasPrice = false;
                for(let price of prices) 
                    if(col.fieldName == price.pricebookId) {
                        this.priceData.push({pbid: price.pricebookId, price: price.price, isEmpty: false, isStandard: price.isStandard});
                        hasPrice = true;
                        if(price.isStandard) 
                            standard = true;
                    }
                if(!hasPrice) {
                    this.priceData.push({pbid: col.fieldName, price: null, isEmpty: true, isStandard: col.isStandard});
                }
            }

        this.hasStandardPrice = standard;

        if (!this.hasStandardPrice) {
            this.priceData = [];
            for(let col of this.columns)
                if(col.isPrice) {
                    this.priceData.push({pbid: col.fieldName, price: null, isEmpty: true, isStandard: col.isStandard});
                }
        }
    }

    descriptionIsFocused = false;

    setDescFocused() {
        this.descriptionIsFocused = true;
    }

    unsetDescFocused() {
        this.descriptionIsFocused = false;
    }

    get name() {
        return this.tabledata.Name;
    }
    set name(value) {
        this.tabledata.Name = value;
    }

    get description() {
        if(this.isEditing || this.descriptionIsFocused)
            return this.tabledata.Description;
        else
            return this.tabledata.Description.substr(0, 15) + '...';
    }
    set description(value) {
        this.tabledata.Description = value;
    }

    @track
    priceData;

    @track
    hasStandardPrice = false;

    tempTableData;

    handlePriceChange(event) {
        event.detail.price == null ? 
        this.deletePrice(this.tabledata.Id.substr(0, 18), event.detail.pbid) :
        this.addPrice(this.tabledata.Id.substr(0, 18), event.detail.pbid, event.detail.price);
    }

    handlePriceChangeOnEditMode(event) {
        let copyData = JSON.parse(JSON.stringify(this.tabledata));
        let didFind = false;
        for(let i = 0; i < copyData.prices.length; i++) {
            if(event.detail.pbid == copyData.prices[i].pricebookId) {
                if(event.detail.price === null) {
                    copyData.prices.splice(i, 1);
                }
                else {
                    copyData.prices[i].price = event.detail.price;
                }
                didFind = true;
                break;
            }
        }
        
        if(!didFind && event.detail.price !== null) {
            copyData.prices.push({pricebookId: event.detail.pbid, price: event.detail.price, isStandard: event.detail.isStandard});
        }

        this.tabledata = copyData;

        this.handleItemChange();
    }

    handleItemChange() {
        const changeItemEvent = new CustomEvent(
            'changeitem', 
            {
                detail: {
                    newData: this.tabledata,
                    oldData: this.oldRecord
                }
            }
        );
        this.dispatchEvent(changeItemEvent);
    }

    deletePrice(id, pbid) {
        deletePricebookEntry({ productId: id,  pricebookId: pbid})
        .then(() => {
            this.dispatchEvent(
                new CustomEvent('refreshprice')    
            );  
        })
        .catch((error) => {
            console.log(error);
            showErrorToast(this, 'Error deleting price');
        });
    }

    addPrice(id, pbid, price) {
        createPriceBookEntry({ productId: id,  pricebookId: pbid, price: price})
        .then(() => {
            this.dispatchEvent(
                new CustomEvent('refreshprice')    
            );  
        })
        .catch((error) => {
            console.log(error);
            showErrorToast(this, 'Error adding price');
        });
    }

    handleAddStandardPrice(event) {

        if(this.isEditing)
            this.handlePriceChangeOnEditMode({detail: {pbid: event.detail, price: 100, isStandard: true}});
        else
            this.addPrice(this.tabledata.Id.substr(0, 18),  event.detail, 100);            
    }

    handleChange(event) {
        let copyData = {...this.tabledata};
        if (event.target.dataset.name === 'Name') {
            copyData.Name = event.target.value;
        } else if (event.target.dataset.name === 'Description') {
            copyData.Description = event.target.value;
        } else if (event.target.dataset.name === 'StockKeepingUnit') {
            copyData.StockKeepingUnit = event.target.value;
        }
        this.tabledata = copyData;
        this.handleItemChange();
    }
    
    @api
    get isEditing() {
        return this._isEditing;
    }
    set isEditing(value) {
        this._isEditing = value;
    }

    _isEditing = false;

    oldRecord;

    saveProduct() {
        this.oldRecord = JSON.parse(JSON.stringify(this.tabledata));
    }

    cancelSaveProduct() {
        this.tabledata = JSON.parse(JSON.stringify( this.oldRecord));
    }

    onEditButtonClick() {
        this.handleItemChange();
        this.isEditing = true;
    }

    onSaveButtonClick() {
        this.isEditing = false;
        let products = [];
        let pbes = [];

        products.push({
            Id: this.tabledata.Id,
            Name: this.tabledata.Name,
            Description: this.tabledata.Description,
            StockKeepingUnit: this.tabledata.StockKeepingUnit
        });

        for(let price of this.tabledata.prices) {
            let id = null;
            for(let pbe of this.pbentries) {
                if(pbe.Pricebook2Id == price.pricebookId && pbe.Product2Id == this.tabledata.Id)
                    id = pbe.Id;
            }
            if(id != null)
                pbes.push({
                    Id: id,
                    Product2Id: this.tabledata.Id,
                    Pricebook2Id: price.pricebookId,
                    UnitPrice: price.price
                });
            else
                pbes.push({
                    Product2Id: this.tabledata.Id,
                    Pricebook2Id: price.pricebookId,
                    UnitPrice: price.price
                });
        }

        saveProductsWithPbes({ products: JSON.stringify(products), pbes: JSON.stringify(pbes) })
        .then(() => {
            showSuccessfullToast(this, 'Product updated');
            this.saveProduct();    
            this.endEdit();
            this.dispatchEvent(
                new CustomEvent('refreshprice')    
            );          
        })  
        .catch((error) => {
            console.log(error);
            showErrorToast(this, 'Error updating product');
            this.cancelSaveProduct();
        });
    }

    handleAddPriceButtonClick(event) {
        let priceDataWithChangedEmptyState = [];
        for(let price of this.priceData) {
            let priceToPush = {...price};
            if(price.pbid == event.detail) {
                priceToPush.isEmpty = false;
                priceToPush.doSetFocus = true;
            }
            priceDataWithChangedEmptyState.push(priceToPush);
        }
        this.priceData = priceDataWithChangedEmptyState;
    }

    endEdit() {
        const endEditEvent = new CustomEvent(
            'endedit', 
            {
                detail: this.tabledata.Id
            }
        );
        this.dispatchEvent(endEditEvent);
    }

    onCancelButtonClick() {
        this.endEdit();
        this.isEditing = false;
        this.cancelSaveProduct();
    }

    onDeleteButtonClick() {
        const deleteItemEvent = new CustomEvent(
            'deleteitem', 
            {
                detail: this.tabledata.Id
            }
        );
        this.dispatchEvent(deleteItemEvent);
    }

    get disabled() {
        return !this.isEditing;
    }

    connectedCallback() {
        if(this.tabledata.inEdit == true) {
            this.isEditing = true;
            this.oldRecord = this.tabledata.oldData;
        }
        else {
            this.saveProduct();
        }
        this.productUrl = this.tabledata.Id;
    }
}