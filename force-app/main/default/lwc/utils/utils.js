import { ShowToastEvent } from 'lightning/platformShowToastEvent';

function showSuccessfullToast(cmp, msg) {
    cmp.dispatchEvent(
        new ShowToastEvent({
            title: 'Success',
            message: msg,
            variant: 'success'
        })
    );
}

function showErrorToast(cmp, msg) {
    cmp.dispatchEvent(
        new ShowToastEvent({
            title: 'Error',
            message: msg,
            variant: 'error'
        })
    );
}

export { showSuccessfullToast, showErrorToast };