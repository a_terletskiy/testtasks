import { LightningElement, api, track } from 'lwc';

const isNumber = '1234567890.';

export default class ProductPrices extends LightningElement {

    @api
    pbid;

    @api
    price;

    @api
    isstandard;

    @api
    isempty;

    @api
    hasstandardprice;

    @api
    dosetfocus;

    @api
    get disabled() {
        return this.isstandard || !this.hasstandardprice;
    }
    set disabled(value) {
        this._disabled = value;
    }

    _disabled;

    get showButton() {
        return this.isempty && (this.hasstandardprice || this.isstandard);
    }

    get showAddStandardButton() {
        return this.isstandard;
    }

    onPriceBlur(event) {
        if(event.target.checkValidity() && !this.disabled && this._disabled) {
            const changePriceEvent = new CustomEvent(
                'pricechange', 
                {
                    detail: {
                        pbid: this.pbid,
                        price: event.target.value == '' ? null : parseFloat(event.target.value)
                    }
                }
            );
            this.dispatchEvent(changePriceEvent);
        }
        else {

        }
    }

    onPriceKeyPress(event) {
        var key = event.key;
        if(!isNumber.includes(key))
            event.preventDefault();
    }

    onPriceChange(event) {
        if(event.target.checkValidity() && !this._disabled) {
            const changePriceEvent = new CustomEvent(
                'pricechangeoneditmode', 
                {
                    detail: {
                        pbid: this.pbid,
                        price: event.target.value == '' ? null : parseFloat(event.target.value)
                    }
                }
            );
            this.dispatchEvent(changePriceEvent);
        }
        else {

        }
    }

    oldprice;

    savePrice() {
        this.oldprice = this.price;
    }

    cancelSavePrice() {
        this.price = this.oldprice;
    }

    onAddPriceButtonClick(event) {
        const addPriceButtonClickEvent = new CustomEvent(
            'addpricebuttonclick', 
            {
                detail: this.pbid
            }
        );
        this.dispatchEvent(addPriceButtonClickEvent);
    }

    onAddStandardPriceButtonClick(event) {
                
        this.isempty = false;        

        const addStandardPriceEvent = new CustomEvent(
            'addstandardprice', 
            {
                detail: this.pbid
            }
        );
        
        this.dispatchEvent(addStandardPriceEvent);
    }

    renderedCallback() {
        if(this.dosetfocus)
        {
            let input = this.template.querySelector('lightning-input');
            input.focus();
        }
    }

    connectedCallback() {
        this.savePrice();
    }

}