public class ProductController {

    @AuraEnabled(cacheable=true)
    public static list<Product2> getProducts(){
        List<Product2> lst = new List<Product2>([SELECT Id, Name, Description, StockKeepingUnit FROM Product2]);
        return lst;
    }

    @AuraEnabled(cacheable=true)
    public static list<PricebookEntry> getPricebookEntries(){
        List<PricebookEntry> lst = new List<PricebookEntry>([SELECT Id, UnitPrice, Product2Id, Pricebook2Id, Pricebook2.isStandard FROM PricebookEntry]);
        return lst;
    }

    @AuraEnabled(cacheable=true)
    public static list<Pricebook2> getPricebooks(){
        List<Pricebook2> lst = new List<Pricebook2>([SELECT Id, Name, isStandard FROM Pricebook2]);
        return lst;
    }

    @AuraEnabled
    public static void createProduct(String name, String description, String sku){
        Product2 prod = new Product2();
        prod.Name = name;
        prod.Description = description;
        prod.StockKeepingUnit = sku;
        insert prod;
    }

    @AuraEnabled
    public static void createPricebook(String name, String description){
        Pricebook2 pricebook = new Pricebook2();
        pricebook.Name = name;
        pricebook.Description = description;
        insert pricebook;
    }

    @AuraEnabled
    public static void deletePriceBookEntry(Id productId, Id pricebookId){

        List<PricebookEntry> pbe = [SELECT Id FROM PricebookEntry 
        WHERE (Product2Id = :productId) AND (Pricebook2Id = :pricebookId)];
        if(pbe.size() > 0)
            delete pbe;
    }

    @AuraEnabled
    public static void createPriceBookEntry(Id productId, Id pricebookId, Decimal price){

        List<PricebookEntry> pbelist = [SELECT Id, Pricebook2Id, Product2Id, UnitPrice FROM PricebookEntry 
        WHERE (Product2Id = :productId) AND (Pricebook2Id = :pricebookId)];

        PricebookEntry pbe = pbelist.size() > 0 ? pbelist[0] : 
        new PricebookEntry(
            Pricebook2Id = pricebookId,
            Product2Id = productId
        );

        pbe.UnitPrice = price;

        upsert pbe;
    }

    @AuraEnabled
    public static void deleteProductWithPbes(Id productId, String standardPbes, String notStandardPbes){

        List<PricebookEntry> standardPbeList = (List<PricebookEntry>) JSON.deserialize(standardPbes, List<PricebookEntry>.class);
        List<PricebookEntry> notStandardPbeList = (List<PricebookEntry>) JSON.deserialize(notStandardPbes, List<PricebookEntry>.class);

        delete notStandardPbeList;
        delete standardPbeList;
        delete [SELECT Id FROM Product2 WHERE Id = :productId];
    }

    @AuraEnabled
    public static void saveProductsWithPbes(String products, String pbes){

        List<Product2> productsList = (List<Product2>) JSON.deserialize(products, List<Product2>.class);
        List<PricebookEntry> pbeList = (List<PricebookEntry>) JSON.deserialize(pbes, List<PricebookEntry>.class);

        upsert productsList;
        upsert pbeList;
    }

}