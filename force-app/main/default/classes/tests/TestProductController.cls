@isTest
public class TestProductController {
   
    @isTest
    static void testGetProducts() {
        Product2 prod = new Product2(Name = 'a');
        insert prod;
        Test.startTest();
            List<Product2> prodlst = ProductController.getProducts();
            System.assertEquals('a', prodlst[0].Name);
        Test.stopTest();
    }

    @isTest
    static void testGetPricebooks() {
        Pricebook2 pb = new Pricebook2(Name = 'pb');
        insert pb;
        Test.startTest();
            List<Pricebook2> pblst = ProductController.getPricebooks();
            System.assertEquals('pb', pblst[0].Name);
        Test.stopTest();
    }

    @isTest
    static void testGetPricebookEntries() {
        Product2 prod = new Product2(Name = 'a');
        Id pricebookId = Test.getStandardPricebookId();
        insert prod;
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id, UnitPrice = 1);
        insert pbe;
        Test.startTest();
            List<PricebookEntry> pbelst = ProductController.getPricebookEntries();
            System.assertEquals(1, pbelst[0].UnitPrice);
        Test.stopTest();
    }

    @isTest
    static void testCreateProduct() {
        Test.startTest();
            ProductController.createProduct('a', 'b', '1');
            Product2 prod = [select id, name from Product2][0];
            System.assertEquals('a', prod.Name);
        Test.stopTest();
    }

    @isTest
    static void testCreatePricebook() {
        Test.startTest();
            ProductController.createPricebook('a', 'b');
            Pricebook2 pb = [select id, name, description from Pricebook2][0];
            System.assertEquals('b', pb.Description);
        Test.stopTest();
    }

    @isTest
    static void testDeletePriceBookEntry() {
        Product2 prod = new Product2(Name = 'a');
        Id pricebookId = Test.getStandardPricebookId();
        insert prod;
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id, UnitPrice = 1);
        insert pbe;
        Test.startTest();
            List<PricebookEntry> pbelst = [select id, unitprice from PricebookEntry];
            System.assertEquals(1, pbelst[0].UnitPrice);
            ProductController.deletePriceBookEntry(prod.Id, pricebookId);
            pbelst = [select id, unitprice from PricebookEntry];
            System.assertEquals(0, pbelst.size());
        Test.stopTest();
    }

    @isTest
    static void testCreatePriceBookEntry() {
        Product2 prod = new Product2(Name = 'a');
        Id pricebookId = Test.getStandardPricebookId();
        insert prod;        
        Test.startTest();
            ProductController.createPriceBookEntry(prod.Id, pricebookId, 12);
            List<PricebookEntry> pbelst = [select id, unitprice from PricebookEntry];
            System.assertEquals(12, pbelst[0].UnitPrice);
        Test.stopTest();
    }

    @isTest
    static void testSaveProductsWithPbes() {
        Product2 prod = new Product2(Name = 'a');
        Id pricebookId = Test.getStandardPricebookId();
        insert prod;
        String prods = '[{"Name":"a"}]';
        String pbes = '[{"Pricebook2Id":"' + pricebookId + '","Product2Id":"' + prod.Id + '","UnitPrice":"22"}]'; 
        Test.startTest();
            ProductController.saveProductsWithPbes(prods, pbes);
            List<Product2> prodlst = [select id, name from Product2];
            List<PricebookEntry> pbelst = [select id, unitprice from PricebookEntry];
            System.assertEquals('a', prodlst[0].Name);
            System.assertEquals(22, pbelst[0].UnitPrice);
        Test.stopTest();
    }

    @isTest
    static void testDeleteProductWithPbes() {
        Product2 prod = new Product2(Name = 'a');
        Id pricebookId = Test.getStandardPricebookId();
        insert prod;
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id, UnitPrice = 1);
        insert pbe;
        String pbes = '[{"Id":"' + pbe.Id + '"}]'; 
        Test.startTest();
            ProductController.deleteProductWithPbes(prod.Id, pbes, '[]');
            List<Product2> prodlst = [select id, name from Product2];
            List<PricebookEntry> pbelst = [select id, unitprice from PricebookEntry];
            System.assertEquals(0, prodlst.size());
            System.assertEquals(0, pbelst.size());
        Test.stopTest();
    }

}